### Install
```` 
npm i https://jjoey@bitbucket.org/orckidlab/vue-modal.git -D
````

### Import
````
import Vue from 'vue';
import vueModal from '@orckid-lab/vue-modal.js';

Vue.use(vueModal);
````

```` 
@import (inline) "~@orckid-lab/vue-modal/dist/app.css";
````

### Usage

```` 
<div id="app">
        <vue-modal ref="myModal">
            <div>
                My customer content
                <a href="javascript:" @click="openModal('myOtherModal')">Open</a>
            </div>
        </vue-modal>
        <vue-modal ref="myOtherModal"></vue-modal>
        <a href="javascript:" @click="openModal('myModal')">Open</a>
    </div>
````

```` 
new Vue({
    el: '#app',
    methods: {
        openModal(ref) {
            this.$refs[ref].open();
        }
    }
});
````