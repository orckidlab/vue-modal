let mix = require('laravel-mix');

if (mix.inProduction()) {
  mix.setPublicPath('dist')
    .js('src/js/vue-modal.js', 'dist/app.js')
    .sass('src/app.scss', 'dist/app.css');

  return;
}

mix.setPublicPath('example/dist')
  .js('src/js/app.js', 'example/dist')
  .sass('src/app.scss', 'example/dist');