import Vue from 'vue';
import vueModal from './vue-modal.js';
import KeenUI from 'keen-ui';
import 'keen-ui/dist/keen-ui.css';

Vue.use(KeenUI);

Vue.use(vueModal);

new Vue({
  el: '#app',
  methods: {
    openModal(ref) {
      this.$refs[ref].open();
    },
    confirm() {
      alert('Confirmed');
    },
    deny() {
      alert('Denied');
    },
    onOpen() {
      console.log('open confirm modal');
    }
  }
});