import VueModal from './component/VueModal';
import VueConfirm from './component/VueConfirm';

export default {
  install(Vue, options) {
    Vue.component('vue-modal', VueModal);

    Vue.component('vue-confirm', VueConfirm);
  }
}